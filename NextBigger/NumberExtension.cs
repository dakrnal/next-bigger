﻿using System;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException($"Value of {nameof(number)} cannot be less zero.");
            }

            if (number == int.MaxValue)
            {
                return null;
            }

            string numberString = number.ToString();
            char[] ar = new char[numberString.Length];
            for (int k = 0; k < numberString.Length; k++)
            {
                ar[k] = numberString[k];
            }

            int n = ar.Length;
            int i;
            for (i = n - 1; i > 0; i--)
            {
                if (ar[i] > ar[i - 1])
                {
                    break;
                }
            }

            if (i == 0)
            {
                return null;
            }
            else
            {
                int x = ar[i - 1], min = i;

                for (int j = i + 1; j < n; j++)
                {
                    if (ar[j] > x && ar[j] < ar[min])
                    {
                        min = j;
                    }
                }

                char temp = ar[i - 1];
                ar[i - 1] = ar[min];
                ar[min] = temp;
                Array.Sort(ar, i, n - i);
                string newnum = string.Empty;
                for (i = 0; i < n; i++)
                {
                    newnum += ar[i];
                }

                return int.Parse(newnum);
            }
        }
    }
}
